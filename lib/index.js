const aws_provider = require('strapi-provider-upload-aws-s3');
const Jimp = require("jimp");

module.exports = {
  init(config) {
    const aws_uploader = aws_provider.init(config);
    const LOGO = './node_modules/strapi-provider-upload-s3-with-watermark/lib/white-logo.png';

    return {
      async upload(file, customParams = {}) {
        if (file.mime.split('/')[0] === 'image' && ((file.hash.split('_').indexOf('nw') === -1) && (Object.keys(file).indexOf('name') !== -1))) {
          const [image, logo] = await Promise.all([
            Jimp.read(file.buffer),
            Jimp.read(LOGO)
          ]);

          logo.resize(image.bitmap.width - (image.bitmap.width * 0.20), Jimp.AUTO);

          const X = (image.bitmap.width / 2) - (logo.bitmap.width / 2);
          const Y = (image.bitmap.height / 2) - (logo.bitmap.height / 2);

          let result = await image.composite(logo, X, Y, {
            mode: Jimp.BLEND_SCREEN,
            opacitySource: 0.2,
            opacityDest: 1
          });
          file.buffer = await result.getBufferAsync(file.mime);
        }

        return aws_uploader.upload(file, customParams);
      },
      delete(file, customParams = {}) {
        return aws_uploader.delete(file, customParams);
      }
    }
  }
}
